<?php

define( 'DVWA_WEB_PAGE_TO_ROOT', '' );
require_once DVWA_WEB_PAGE_TO_ROOT . 'dvwa/includes/dvwaPage.inc.php';

dvwaPageStartup( array( 'authenticated', 'phpids' ) );

$page = dvwaPageNewGrab();
$page[ 'title' ]   = 'Welcome' . $page[ 'title_separator' ].$page[ 'title' ];
$page[ 'page_id' ] = 'home';

$page[ 'body' ] .= "
  <div class=\"body_padded\">
        <h1>Bienvenido a la aplicación más segura</h1>
        <p>Este es un sistema 100% seguro, certificado por ACME Inc.</p>
</div>";

dvwaHtmlEcho( $page );

?>
