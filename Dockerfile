FROM tutum/lamp

MAINTAINER Richard

RUN rm -rf /app && \
    apt-get update && \
    apt-get install -y curl wget php5-gd && \
    rm -rf /var/lib/apt/lists/*

RUN apt-get install -y git

ARG CACHEBUST=1
RUN git clone https://gitlab.com/public-lab/dvwa-sqli /app

COPY setup_dvwa.sh /tmp/

RUN chmod u+x /tmp/setup_dvwa.sh && \
    /tmp/setup_dvwa.sh

EXPOSE 80 3306

CMD ["/bin/sh", "-c", "chmod u+x /app/app.sh; /app/app.sh"]
